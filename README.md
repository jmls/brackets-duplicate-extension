brackets-duplicate-extension
============================

*Forked and customized by whoGloo for nodeSpeed Development - Brackets in a Docker container and browser*

# File & Folder Duplicating Extension for Brackets
An extension for [Brackets](https://github.com/adobe/brackets/) that provides the duplicate functionality to duplicate files and folders in the project view. Project was inspired by Jeff Booher's [Bracket's New Projector Creator Extension](https://github.com/JeffryBooher/brackets-newproject-extension).

### How to Install
1. Select **File > Extension Manager...**
2. Search for this extension.
3. Click on the **Install** button.

### How to Use Extension
Duplicate - Right click on a file or folder in the project view and select "Duplicate" from the context menu.

Copy or Move - Right click on a file or folder and select "Mark" to mark the file/folder to be copied or moved. Then right click on a file or folder at your desired destination and then select "Move to Here" or "Copy to Here".

### License
MIT-licensed.

### Compatibility
Tested on nodeSpeed Development with Brackets 1.8 
