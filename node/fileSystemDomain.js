/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50, white: true */
/*global require, exports*/

(function () {

    'use strict';

    // Node Modules
    var fs = require('fs');

    /**
     * @private
     * Handler function for copying file on the disk.
     * @param {string} Path to source file.
     * @param {string} Destination where the file is copied.
     * @param {string} Callback.
     */
    function copyFile(source, destination, cb) {

        var destDir = destination.substr(0, destination.lastIndexOf('/'));

        if (!fs.existsSync(destDir)) {
            fs.mkdirSync(destDir);
        }

        var rd = fs.createReadStream(source);
        var wr = fs.createWriteStream(destination);

        wr.on('close', cb);

        rd.pipe(wr);
    }

    // Init
    function init(domainManager) {

        if (!domainManager.hasDomain('fileSystemDomain')) {

            domainManager.registerDomain('fileSystemDomain', {
                major: 0,
                minor: 1
            });
        }

        domainManager.registerCommand('fileSystemDomain', 'copyFile', copyFile, true, 'Copy a file on server', [{
            name: 'source',
            type: 'string',
            decription: 'Path to source file'
        }, {
            name: 'destination',
            type: 'string',
            decription: 'Destination where the file is copied'
        }], [{
            name: 'result', // return value
            type: '{source: string, destination: string, result: string}',
            description: 'Callback after copy'
        }]);
    }

    exports.init = init;
}());
